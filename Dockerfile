FROM alpine:3.15

RUN apk add --no-cache unbound
COPY unbound.conf /etc/unbound/unbound.conf

ENTRYPOINT ["/usr/sbin/unbound", "-d", "-p"]
