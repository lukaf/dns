PODMAN := podman --remote -c rpi

.PHONY: build
build:
	$(PODMAN) build -t dns .

.PHONY: deploy
deploy:
	-$(PODMAN) container exists dns && $(PODMAN) rm -f dns
	$(PODMAN) run -d --name dns -p 5353:53/tcp -p 5353:53/udp dns
